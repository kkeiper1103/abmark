<?php

namespace App\Console\Commands;

use AdamBrett\ShellWrapper\Runners\ShellExec;
use AdamBrett\ShellWrapper\Command\Builder as CommandBuilder;
use App\UrlWrapper;
use Illuminate\Console\Command;

class BenchWebsite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bench {url} {--t|total=1000} {--i|increments=100}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Benchmarks website using Apache Bench';


    /**
     * Create a new command instance.
     *
     */
    public function __construct(  )
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $site = new UrlWrapper( $this->argument('url') );

        $total = $this->option('total');
        $inc = $this->option('increments');

        $metafile = $site->path() . "/meta.dat";

        $meta = [
            "date" => date('c'),
            "url" => $site,
            "total_connections" => $total,
            "increments_by" => $inc
        ];

        // @TODO: find more efficient way to count progress sections
        $pb_count = $total / $inc;
        for($i = $inc; $i <= $total; $i += $inc) {
            for($j = $inc; $j <= $total; $j += $inc) {
                if( $j > $i )
                    break;

                $pb_count++;
            }
        }

        //
        $bar = $this->output->createProgressBar( $pb_count );


        // create base folder for tests
        if( !is_dir( $site->path() ) ) {
            mkdir( $site->path(), 0775, true );
        }
        else {
            // remove all previous tests so no results are old
            array_map('unlink', glob( $site->path() . "/*.dat" ));
        }

        // get time taken for tests
        $t1 = microtime(true);

        // number of total connections
        for($i = $inc; $i <= $total; $i += $inc) {


            // step for concurrent connections
            for($j = $inc; $j <= $total; $j += $inc) {

                // don't worry about testing 100 connections with 200 concurrent... etc
                if( $j > $i )
                    break;

                $shell = new ShellExec();
                $command = new CommandBuilder('ab');

                // how many total connections to try, and how many to try at a time
                $command->addFlag('n', $i)->addFlag('c', $j)->addParam( (string) $site );

                $this->info(PHP_EOL . $command);

                //
                file_put_contents( $site->test_path($i, $j), $shell->run($command) );

                $bar->advance();
            }

            $bar->advance();
        }

        $meta['time_elapsed'] = microtime(true) - $t1;
        $this->writeToMetafile($metafile, $meta);


        //
        $bar->finish();
    }

    /**
     * @param $metafile
     * @param array $values
     */
    public function writeToMetafile($metafile, array $values = []) {
        file_put_contents($metafile, serialize($values));
    }
}