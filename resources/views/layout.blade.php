<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $title or "Apache Benchmarks" }}</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <style>
        body {
            padding-bottom: 51px;
        }

        @media screen and (max-width: 1199px) {
            body {
                padding-bottom: 258px;
            }
        }

        @media screen and (max-width: 767px) {
            .navbar.navbar-fixed-bottom {
                padding: 0 15px;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1199px) {

            .navbar-fixed-bottom .form-group label,
            .navbar-fixed-bottom .form-group input {
                display: block;
                width: 100%;
            }

            .navbar-fixed-bottom .form-group {
                margin-bottom: 10px;
                display: block;
            }
        }

        @media screen and (min-width: 1200px) {
            .navbar-fixed-bottom .form-group {
                margin-right: 20px;
            }
        }

        table.table td a {
            color: black;
        }

    </style>
</head>
<body>

<main class="container">

    <div class="page-header">
        <h1 class="title">@yield('title', "Available Tests")</h1>
    </div>

    <div class="row">
        <div class="col-md-9 col-md-push-3">
            @yield('main', "<h3 class='title'>Select a Test</h3>")
        </div>
        <div class="col-md-3 col-md-pull-9">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Available Tests</h3>
                </div>
                <ul class="list-group">
                    @foreach(get_tests() as $t)
                        <li class="list-group-item">
                            <a href="/{{ base64_encode($t) }}">{{ $t }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>

        </div>
    </div>
</main>


<div class="navbar navbar-default navbar-fixed-bottom">
    <div class="container">

        <div class="row">
            <form action="/bench" method="post" class="navbar-form">
                <div class="form-group">
                    <label for="total" class="form-label">Total Connections</label>
                    <input type="number" placeholder="1000" name="total" id="total" class="form-control" step="100"/>
                </div>

                <div class="form-group">
                    <label for="increments" class="form-label">Increment By</label>
                    <input type="number" placeholder="100" name="increments" id="increments" class="form-control" step="10">
                </div>

                <div class="form-group">
                    <label for="url" class="form-label">URL</label>
                    <input type="url" name="url" id="url" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary">Benchmark Site</button>
            </form>
        </div>


    </div>
</div>


</body>
</html>