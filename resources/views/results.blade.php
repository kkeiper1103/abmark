@extends('layout')

@section('title', "Statistics for " . $url)

@section('main')

    <p>These are worst-case results, meaning that the shown value is the absolute longest it took in the test, when given the combination of X and Y.</p>

    <p>X-Axis Represents the Number of Concurrent Connections.</p>
    <p>Y-Axis Represents the Number of Total Connections.</p>

    <p>Anything past {{ env('MAX_TIME_THRESHOLD') }} milliseconds is too slow.</p>


    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Test Results</h3>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    @foreach(end($results) as $k => $v)
                        <th>{{ $k }} cc</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($results as $total => $concurrent)
                    <tr>
                        <th>{{ $total }} tc</th>

                        @foreach($concurrent as $k => $v)
                            <?php $percentiles = $v->percentiles ?>
                            {{--<td style="{{ !$v->isValid() ? "background-color: #ff0000; font-weight: bold;" : '' }}">--}}
                            <td {!! print_styles($results, $v) !!}>
                                <a href="/{{ base64_encode($url) }}/{{ "{$total}:{$k}" }}">
                                    {{ $v->isValid() ? end($percentiles) . " ms" : "Test Failed" }}
                                </a>
                            </td>
                        @endforeach

                        <?php $cells = count($concurrent) ?>
                        @while( count(end($results)) - $cells > 0 )
                            <td>N/a</td>

                            <?php $cells++ ?>
                        @endwhile
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="panel-footer">
            <p>Results current as of <strong>{{ date( 'M j, Y @ g:i:s A', strtotime($meta['date'])) }}</strong>.</p>
            <p>Time Elapsed while testing: <strong>{{ (int) ($meta['time_elapsed'] / 60) }}m {{ (int) $meta['time_elapsed'] % 60 }}s</strong>.</p>
        </div>
    </div>
@stop