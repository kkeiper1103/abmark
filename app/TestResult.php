<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 3/4/16
 * Time: 9:04 AM
 */

namespace App;

class TestResult
{
    /**
     * @var string
     */
    private $raw;

    /**
     * @var array
     */
    private $props = [];

    /**
     * TestResult constructor.
     * @param $raw string The Raw Output of the `ab` program
     */
    public function __construct( $raw ) {

        $this->raw = $raw;

        $this->parse();
    }

    /**
     * @param $prop
     * @return null
     */
    public function __get($prop) {
        if( isset($this->props[$prop]) )
            return $this->props[$prop];

        return null;
    }

    /**
     * @param $prop
     * @param $value
     */
    public function __set($prop, $value) {
        $this->props[$prop] = $value;
    }

    /**
     * @return void
     */
    private function parse() {

        // parse the colon delimited properties into the props array
        foreach( explode(PHP_EOL, $this->raw) as $lno => $line ) {

            // ignore lines 1 - 7
            if( in_array($lno, range(0, 6)) || empty($line)) {
                continue;
            }

            //
            if( strpos($line, ': ') !== false ) {
                $parts = array_map('trim', explode(':', $line));

                $key = str_replace(' ', '_', strtolower($parts[0]));

                $this->props[$key] = $parts[1];
            }
        }

        // next, deal with the connection times table
        $start = strpos($this->raw, "Connection Times (ms)");
        $end = strpos($this->raw, "Percentage of the requests");

        foreach(explode(PHP_EOL, substr($this->raw, $start, $end)) as $lno => $line) {

            if(empty($line) || strpos($line, ': ') === false) {
                continue;
            }

            $parts = array_map('trim', explode(':', $line));

            $key = str_replace(' ', '_', strtolower($parts[0]));

            $this->props[$key] = array_values(array_filter(explode(' ', $parts[1])));
        }



        // finally, parse the percentiles into a table
        $start = $end;
        $this->props['percentiles'] = [];

        foreach(array_map('trim', explode(PHP_EOL, substr($this->raw, $start))) as $lno => $line ) {

            // skip first line
            if($lno === 0 || empty($line))
                continue;


            $parts = sscanf($line, "%d%% %d");

            $this->props['percentiles'][$parts[0]] = $parts[1];
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->raw;
    }

    /**
     *
     */
    public function isValid() {
        return preg_match("/^Total of [\d]+ requests completed$/mi", $this->raw) === 0;
    }
}