@extends('layout')



@section("main")

    <div class="page-header">
        <h1 class="title">Available Tests</h1>
    </div>

    <ul class="list-group">
        @foreach($tests as $t)
        <li class="list-group-item">
            <a href="/{{ base64_encode($t) }}">{{ $t }}</a>
        </li>
        @endforeach
    </ul>
@stop