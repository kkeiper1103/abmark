<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 3/7/16
 * Time: 9:53 AM
 */

namespace App\Exceptions;


class InvalidUrlException extends \Exception
{
    /**
     * InvalidUrlException constructor.
     * @param string $url
     */
    public function __construct( $url ) {
        parent::__construct("Invalid URL: " . $url);
    }
}