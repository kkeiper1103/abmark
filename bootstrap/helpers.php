<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 3/4/16
 * Time: 10:09 AM
 */
use App\TestResult;

/**
 * @return array
 */
function get_tests() {
    $tests = array_diff(scandir( storage_path('app/tests') ), ['.', '..', '.gitignore']);

    return array_map(function($t){ return new \App\UrlWrapper( base64_decode($t) ); }, $tests);
}

/**
 * @param $b64
 * @return mixed
 * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
 */
function load_results($b64) {
    $path = storage_path("app/{$b64}");

    if( !is_readable( $path ) ) {
        throw new \Illuminate\Contracts\Filesystem\FileNotFoundException();
    }

    $results = unserialize( file_get_contents( $path ) );


    //
    foreach($results as $totalConnections => $increments) {
        foreach($increments as $concurrent => $raw) {

            $test = new TestResult($raw);
            $results[$totalConnections][$concurrent] = $test;

        }
    }

    return $results;
}

/**
 * @param array $results
 * @param TestResult $test
 * @return string
 */
function print_styles( array $results, TestResult $test ) {
    if( !$test->isValid() ) {
        $color = getHslColor(1);
        return "style=\"background-color: {$color}; font-weight: bold;\"";
    }



    // $max = find_max($results);
    $max = env('MAX_TIME_THRESHOLD'); // 4s is threshold for acceptable response time

    // $min = find_min($results);
    $min = 0;

    //
    $p = $test->percentiles;

    $color = getHslColor( (end($p) - $min) / ($max - $min) );

    return "style=\"background-color: {$color};\"";
}

/**
 * @param array $results
 */
global $max; $max = null;
function find_max( array $results ) {
    global $max;
    if( ! is_null($max) ) {
        return $max;
    }

    $tmp = 0;
    foreach( $results as $total => $array ) {
        foreach( $array as $concurrency => $test ) {
            if(!$test->isValid())
                continue;

            $p = $test->percentiles;
            if( end($p) > $tmp )
                $tmp = end($p);
        }
    }

    $max = $tmp;
    return $max;
}

/**
 * @param array $results
 */
global $min; $min = null;
function find_min( array $results ) {
    global $min;
    if( ! is_null($min) ) {
        return $min;
    }

    $tmp = PHP_INT_MAX;
    foreach( $results as $total => $array ) {
        foreach( $array as $concurrency => $test ) {
            if(!$test->isValid())
                continue;

            $p = $test->percentiles;
            if( end($p) < $tmp )
                $tmp = end($p);
        }
    }

    $min = $tmp;
    return $min;
}

/**
 * @param $decimal
 * @return string
 */
function getHslColor( $decimal ) {

    // do some bounds checking
    if( $decimal < 0 ) {
        $decimal = 0;
    }
    else if( $decimal > 1 ) {
        $decimal = 1;
    }

    // 120 means '0' starts at green and goes to red
    // 180 means '0' starts at bluish green and goes to red
    // 240 means '0' starts at blue and goes to red

    $hue = (string) ((1 - $decimal) * 180);

    return implode('', ["hsl(", $hue, ", 85%, 35%)"]);
}