<?php
/**
 * Created by PhpStorm.
 * User: kkeiper
 * Date: 3/5/2016
 * Time: 12:46 PM
 */

namespace App;


use App\Exceptions\InvalidUrlException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class UrlWrapper
{
    private $url;

    /**
     * @param $url
     * @throws InvalidUrlException
     */
    public function __construct( $url ) {

        if( filter_var($url, FILTER_VALIDATE_URL) === false ) {
            throw new InvalidUrlException( $url );
        }

        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function __toString() {
        return $this->url;
    }

    /**
     * @return string
     */
    public function path() {
        return storage_path( implode(["app", "tests", base64_encode($this->url)], DIRECTORY_SEPARATOR) );
    }

    /**
     * @param $total
     * @param $concurrency
     * @return string
     */
    public function test_path( $total, $concurrency ) {
        return $this->path() . "/{$total}-{$concurrency}.dat";
    }

    /**
     * @param $total
     * @param $concurrency
     * @return string
     * @throws FileNotFoundException
     */
    public function getTestResult($total, $concurrency) {
        $path = $this->test_path($total, $concurrency);

        if( is_readable( $path ) ) {
            return file_get_contents($path);
        }

        throw new FileNotFoundException();
    }
}