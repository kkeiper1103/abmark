# Abmark

Abmark is a utility site that wraps the Apache Bench command to automate testing a website under given settings.

You give it a URL, the # of total connections and the amount to increment by, and the site will then run those tests and
write the results for you to view later. It color codes the results so that you can quickly tell if a certain configuration
is bad news for your site.

# todo
Color Coding. 