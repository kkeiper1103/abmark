<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Input;

ini_set("xdebug.var_display_max_data", -1);
ini_set("xdebug.var_display_max_depth", -1);

date_default_timezone_set( env("TIMEZONE", "UTC") );

/**
 *
 */
$app->post('bench', function() use ($app) {

    $url = Input::get("url");
    $total = (int) Input::get("total");
    $inc = (int) Input::get("increments");

    $url = rtrim($url, '/') . '/'; // standardize the '/' trailing slash

    Artisan::call('bench', [
        'url' => $url,
        '--total' => $total,
        '--increments' => $inc
    ]);



    return redirect()->to('/');
});

/**
 *
 */
$app->get("/{b64}", function($b64) use ($app) {

    // if it's not a valid b64 string, it's a 404
    if( ($url = base64_decode($b64)) === false || filter_var($url, FILTER_VALIDATE_URL) === false ) {
        throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
    }

    //
    $url = new \App\UrlWrapper( $url );

    $results = [];

    // $files = array_diff(scandir( $url->path() ), ['.', '..', '.gitignore']);
    $wd = getcwd();

    chdir( $url->path() );
    $files = glob( "*-*.dat" );
    chdir($wd);

    foreach($files as $file) {
        list($total, $concurrent) = sscanf($file, "%d-%d");

        if( !isset($results[$total]) )
            $results[$total] = [];

        $results[$total][$concurrent] = new \App\TestResult( $url->getTestResult($total, $concurrent) );
    }

    //
    ksort($results);
    array_walk($results, 'ksort');


    // load meta information
    try {
        $meta = unserialize( file_get_contents($url->path() . "/meta.dat") );

        if($meta === false)
            throw new Exception();
    }
    catch(\Exception $e) {
        $meta = [];
    }



    return view('results', [
        'url' => $url,
        'results' => $results,
        'meta' => $meta
    ]);
});

/**
 *
 */
$app->get("/{b64}/{cell}", function($b64, $cell) use ($app) {
    // if it's not a valid b64 string, it's a 404
    if( ($url = base64_decode($b64)) === false || filter_var($url, FILTER_VALIDATE_URL) === false ) {
        throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
    }

    //
    $url = new \App\UrlWrapper( $url );

    list($total, $concurrent) = explode(':', $cell);

    if( file_exists( $url->test_path($total, $concurrent) ) ) {
        return view('result', [
            'url' => base64_decode($b64),
            'result' => new \App\TestResult( $url->getTestResult($total, $concurrent) )
        ]);
    }

    throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
});

/**
 *
 */
$app->get('/', function () use ($app) {

    $test_folder = storage_path('app/tests');

    if( !is_dir( $test_folder ) ) {
        mkdir( $test_folder );
    }

    $tests = array_diff(scandir( $test_folder ), ['.', '..', '.gitignore']);

    return view("layout", [
        'tests' => array_map(function($t){ return new \App\UrlWrapper( base64_decode($t) ); }, $tests)
    ]);
});

